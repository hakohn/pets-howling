using System;

namespace Random_CSharp_stuff
{
    class Master
    {
        private string name;
        private Animal[] pets;


        public Master()
        {
            Start();
        }

        // Initalizing the class values using this function
        private void Start()
        {
            name = "Hakohn";

            pets = new Animal[5];

            pets[0] = new Wolf();
            pets[1] = new Bear();
            pets[2] = new Mustang();
            pets[3] = new Rat();
            pets[4] = new Raven();
        }

        public void CallPets()
        {
            Console.WriteLine(this.name + " calls all his pets and orders them to speak. Now! ");
            foreach(Animal pet in pets)
            {
                Console.WriteLine("The " + pet.Name.ToLower() + " makes " + pet.HowlSound.ToLower());
            }
        }
    }

    abstract class Animal
    {
        private string sound = null;
        public string HowlSound
        {
            get
            {
                return sound;
            }
            set
            {
                sound = value;
            }
        }
        public string Name
        {
            get
            {
                return this.GetType().Name;
            }
        }

        protected Animal()
        {
            Start();
        }

        protected abstract void Start();
    }

    class Wolf : Animal
    {
        protected override void Start()
        {
            HowlSound = "Awwooooooo!";
        }
    }
    class Bear : Animal
    {
        protected override void Start()
        {
            HowlSound = "Grooooowwwwwl!";
        }
    }
    class Mustang : Animal
    {
        protected override void Start()
        {
            HowlSound = "Neiiiigghhhhh!";
        }
    }
    class Rat : Animal
    {
        protected override void Start()
        {
            HowlSound = "Squeeeaak!";
        }
    }
    class Raven : Animal
    {
        protected override void Start()
        {
            HowlSound = "Crooo croooo!";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Master master = new Master();
            master.CallPets();

            Console.ReadKey(); 
        }
    }
}
